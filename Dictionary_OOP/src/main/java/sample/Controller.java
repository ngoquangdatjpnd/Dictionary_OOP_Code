package main.java.sample;

import javafx.fxml.Initializable;
import javafx.scene.control.*;
import main.java.DictionaryClass.DictionaryManagement;

import java.net.URL;
import java.util.ResourceBundle;

import static main.java.DictionaryClass.Dictionary.*;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.fxml.FXML;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;
import main.java.DictionaryClass.Word;

import java.io.IOException;
import java.util.ArrayList;

public class Controller implements Initializable {

    //Variable.

    @FXML
    private AnchorPane Top_App;

    // tìm từ.

    @FXML
    private TextField Search_Text_Field;

    @FXML
    private JFXButton Search_Cancel;

    @FXML
    private JFXButton Search_Button;

    @FXML
    private JFXButton Search_History;

    @FXML
    private JFXListView<String> Search_ListView;

    @FXML
    private JFXButton Search_Left;

    @FXML
    private JFXButton Search_Right;

    @FXML
    private JFXButton DeleteWord;

    @FXML
    private JFXButton LikeButton;

    @FXML
    private MaterialDesignIconView LikeButtonColor;

    @FXML
    private JFXButton FixWord;


    /**
     * Them tu.
     */
    @FXML
    private AnchorPane AddWordTab;

    @FXML
    private Text TypeDictionary;

    @FXML
    private JFXButton ChangeDicButton;

    @FXML
    private TextField AddWord_Target;

    @FXML
    private CheckBox AdjCheck;

    @FXML
    private CheckBox NCheck;

    @FXML
    private CheckBox VCheck;

    @FXML
    private CheckBox AdvCheck;

    @FXML
    private TextField Explain1;

    @FXML
    private TextField Explain2;

    @FXML
    private TextField Explain3;

    @FXML
    private JFXButton Save_AddTab;

    @FXML
    private JFXButton Cancel_AddTab;

    @FXML
    private ListView<String> AddWord_ListView;

    @FXML
    private WebView AddWord_Result;

    /**
     * Liked Words.
     */
    @FXML
    private AnchorPane FavoriteWord;

    @FXML
    private ListView<String> List_View_Favorite;

    @FXML
    private Text TypeDictionary1;

    @FXML
    private JFXButton ChangeDicButton1;


    @FXML
    private WebView Result_tab;

    @FXML
    private JFXButton Sound_FT;


    //kết quả.

    @FXML
    private WebView Result_WebView;

    //GG translate.
    @FXML
    private AnchorPane GGTab;

    @FXML
    private TextArea GGDichNhap;

    @FXML
    private TextArea GGDichNGhia;

    @FXML
    private JFXButton GGDich_Change;

    @FXML
    private Text GGDichText;

    @FXML
    private Text GGDichText2;

    @FXML
    private JFXButton GG_Sound;

    @FXML
    private JFXButton GG_Sound2;

    // Thanh Menu
    @FXML
    private Button AV_Button;

    @FXML
    private Button VA_Button;

    @FXML
    private Button Add_Word;

    @FXML
    private Button Liked_Word;

    @FXML
    private JFXButton Sound_Button;

    @FXML
    private Button GGDich;



    //list file.
    DictionaryManagement dcm = new DictionaryManagement();
    String filename = "src/main/resources/Text_file/E_V.txt";
    String filename2 = "src/main/resources/Text_file/V_E.txt";
    String deletedFile = "src/main/resources/Text_file/deletedFileE_V.txt";
    String deletedFile2 = "src/main/resources/Text_file/deletedFileV_E.txt";
    String historyFile = "src/main/resources/Text_file/historyFileE_V.txt";
    String historyFile2 = "src/main/resources/Text_file/historyFileV_E.txt";
    String favoriteFile = "src/main/resources/Text_file/favoriteFileE_V.txt";
    String favoriteFile2 = "src/main/resources/Text_file/favoriteFileV_E.txt";

    int state = 0;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //khoi tao.
        indexBack = 0;
        //load tu dien
        Result_WebView.getEngine().loadContent("");

        dictionary_favorite = dcm.insertFromFile2(favoriteFile);
        dictionary_history = dcm.insertFromFile2(historyFile);
        dictionary_list = dcm.insertFromFile(filename);
        dcm.addFavoriteWordFromFile(favoriteFile, dictionary_list);

        //Them menu button vao danh sach MenuTab.
        ArrayList <Button> MenuTab = new ArrayList<>();
        MenuTab.add(AV_Button);
        MenuTab.add(VA_Button);
        MenuTab.add(Add_Word);
        MenuTab.add(Liked_Word);
        MenuTab.add(GGDich);
        Chosen_Button(MenuTab, AV_Button);
        Search_ListView.setStyle("-fx-font-size: 15px; -fx-font-family: 'SketchFlow Print';");

        //khoi tao thanh tab.
        ArrayList<AnchorPane> tablist = new ArrayList<>();
        tablist.add(Top_App);
        tablist.add(AddWordTab);
        tablist.add(FavoriteWord);
        tablist.add(GGTab);

        //switch type dic.
        //Anh V.
        AV_Button.setOnMouseClicked(event -> {
            state = 0;
            Chosen_Button(MenuTab, AV_Button);
            Initial(tablist, Top_App);
            dictionary_list = dcm.insertFromFile(filename);
            dcm.addFavoriteWordFromFile(favoriteFile, dictionary_list);
            dictionary_history = dcm.insertFromFile2(historyFile);
        });

        //V Anh.
        VA_Button.setOnMouseClicked(event -> {
            state = 1;
            Chosen_Button(MenuTab, VA_Button);
            Initial(tablist, Top_App);
            dictionary_favorite = dcm.insertFromFile2(favoriteFile2);
            dictionary_list = dcm.insertFromFile(filename2);
            dcm.addFavoriteWordFromFile(favoriteFile2, dictionary_list);
            dictionary_history = dcm.insertFromFile2(historyFile2);

        });

        Add_Word.setOnMouseClicked(event -> {
            Chosen_Button(MenuTab, Add_Word);
            Initial(tablist, AddWordTab);
            state = 0;
        });

        Liked_Word.setOnMouseClicked(event -> {
            dictionary_list = dcm.insertFromFile(filename);
            dcm.addFavoriteWordFromFile(favoriteFile, dictionary_list);

            Chosen_Button(MenuTab, Liked_Word);
            dictionary_favorite = dcm.insertFromFile2(favoriteFile);
            Initial(tablist, FavoriteWord);
            state = 0;
        });

        FixWord.setOnMouseClicked(event -> {
            String s = Search_Text_Field.getText();
            if (s.length() > 0) {
                Initial(tablist, AddWordTab);
                AddWord_Target.setText(s);
                Chosen_Button(MenuTab, Add_Word);
                Search_A_Word2(s, dcm, AddWord_Result);
                if (state == 0) {
                    dictionary_list = dcm.insertFromFile(filename);
                } else {
                    TypeDictionary.setText("Việt-Anh");
                    dictionary_list = dcm.insertFromFile(filename2);
                }
            }

        });

        Cancel_AddTab.setOnMouseClicked(event -> Initial(tablist, AddWordTab));

        LikeButton.setOnMouseClicked(event -> {
            String searchWord = Search_Text_Field.getText();
            Word newWord = dcm.SearchNewWord(searchWord, dictionary_list);
            if (newWord.isLiked()) {
                LikeButtonColor.setFill(Color.BLACK);
                newWord.setLiked(false);
                if (state == 0) {
                    dcm.removeLikedWord(searchWord, dictionary_list, dictionary_favorite);
                    try {
                        dcm.DeleteAWordInFile2(favoriteFile, searchWord);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    dcm.removeLikedWord(searchWord, dictionary_list, dictionary_favorite);
                    try {
                        dcm.DeleteAWordInFile2(favoriteFile2, searchWord);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                LikeButtonColor.setFill(Color.RED);
                newWord.setLiked(true);
                dcm.addLikedWord(searchWord, dictionary_list, dictionary_favorite);
                if (state == 0) {
                    try {
                        dcm.addWordToFile2(favoriteFile, searchWord, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (state == 1) {
                    try {
                        dcm.addWordToFile2(favoriteFile2, searchWord, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        });

        DeleteWord.setOnMouseClicked(event -> Initial(tablist, Top_App));


        //Typing a word.
        Search_Text_Field.setOnKeyReleased(event -> {
            if (Search_Text_Field.getLength() != 0) {
                String Search_Word = Search_Text_Field.getText();
                if (event.getCode().equals(KeyCode.ENTER)) {
                    Search_A_Word(Search_Word, dcm, Result_WebView);
                    Word Search_Word2 = new Word(Search_Word, dcm.Dictionary_Lookup(Search_Word, dictionary_list));
                    dcm.addWordBackList(Search_Word2);
                }
                else {
                    Search_Cancel.setVisible(true);
                    Search_ListView.setVisible(true);
                    recommendTask(Search_Word, 10, Search_ListView);
                }
            }
            else {
                Search_Cancel.setVisible(false);
                Search_ListView.setVisible(false);
            }
        });

        AddWord_Target.setOnKeyReleased(event -> {
            if (AddWord_Target.getLength() != 0) {
                String Search_Word = AddWord_Target.getText();
                if (event.getCode().equals(KeyCode.ENTER)) {
                    Search_A_Word(Search_Word, dcm, AddWord_Result);
                    AddWord_ListView.setVisible(false);
                }
                else {
                    AddWord_ListView.setVisible(true);
                    if (AddWord_ListView.getItems().size() == 0) {
                        AddWord_ListView.setVisible(false);
                    }
                    Search_A_Word2(Search_Word, dcm, AddWord_Result);
                    recommendTask(Search_Word, 10, AddWord_ListView);
                }
            }
            else {
                AddWord_Result.getEngine().loadContent("Hãy nhập 1 từ!");
                AddWord_ListView.setVisible(false);
            }
        });

        //huy tim kiem dua thanh tim kiem ve "".
        Search_Cancel.setOnMouseClicked(event -> {
            Search_Text_Field.setText("");
            Search_Cancel.setVisible(false);
            Search_ListView.setVisible(false);
        });

        //chon tu tim kiem.
        Search_ListView.setOnMouseClicked(event -> {
            String Chosen_Word = Search_ListView.getSelectionModel().getSelectedItem();
            Search_A_Word(Chosen_Word, dcm, Result_WebView);
            Word Search_Word2 = new Word(Chosen_Word, dcm.Dictionary_Lookup(Chosen_Word, dictionary_list));
            dcm.addWordBackList(Search_Word2);
        });

        AddWord_ListView.setOnMouseClicked(event -> {
            String Chosen_Word = AddWord_ListView.getSelectionModel().getSelectedItem();
            AddWord_Target.setText(Chosen_Word);
            Search_A_Word2(Chosen_Word, dcm, AddWord_Result);
            AddWord_ListView.setVisible(false);
        });

        //nhan vao button tim kiem.
        Search_Button.setOnMouseClicked(event -> {
            if (Search_Text_Field.getLength() != 0) {
                String Search_Word = Search_Text_Field.getText();
                Search_A_Word(Search_Word, dcm, Result_WebView);
                Word Search_Word2 = new Word(Search_Word, dcm.Dictionary_Lookup(Search_Word, dictionary_list));
                dcm.addWordBackList(Search_Word2);
            }
        });

        Search_Left.setOnMouseClicked(event -> {
            if (indexBack > 0) {
                indexBack --;
                Search_A_Word(dictionary_backup.get(indexBack).getWord_target(), dcm, Result_WebView);
            }
        });

        Search_Right.setOnMouseClicked(event -> {
            if (indexBack < dictionary_backup.size() - 1) {
                indexBack ++;
                Search_A_Word(dictionary_backup.get(indexBack).getWord_target(), dcm, Result_WebView);
            }
        });

        ChangeDicButton.setOnMouseClicked(event -> {
            if (TypeDictionary.getText().equals("Anh-Việt")) {
                TypeDictionary.setText("Việt-Anh");
                state = 1;
                dictionary_list = dcm.insertFromFile(filename2);
            } else {
                TypeDictionary.setText("Anh-Việt");
                state = 0;
                dictionary_list = dcm.insertFromFile(filename);
            }
            AddWord_Target.setText("");
            AddWord_Result.getEngine().loadContent(" ");
            AdvCheck.setSelected(false);
            VCheck.setSelected(false);
            AdjCheck.setSelected(false);
            NCheck.setSelected(false);
            AddWord_ListView.setVisible(false);

        });

        Search_History.setOnMouseClicked(event -> {
            if (state == 0) {
                dictionary_history = dcm.insertFromFile2(historyFile);
            } else {
                dictionary_history = dcm.insertFromFile2(historyFile2);
            }
            Search_ListView.getItems().clear();
            ArrayList<String> list = new ArrayList<>();
            for (int i = dictionary_history.size() - 1; i >= 0; i--) {
                list.add(dictionary_history.get(i));
            }
            Search_ListView.getItems().addAll(list);
            Search_ListView.setVisible(true);
        });

        DeleteWord.setOnMouseClicked(event -> {
            String word = Search_Text_Field.getText();
            Word sword = dcm.SearchNewWord(word, dictionary_list);
            if (state == 0) {
                deleteAWord(filename, deletedFile, sword);
                dictionary_list = dcm.insertFromFile(filename);
                dictionary_deleted = dcm.insertFromFile2(deletedFile);
                try {
                    dcm.DeleteAWordInFile2(favoriteFile, word);
                    dcm.DeleteAWordInFile2(historyFile, word);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                deleteAWord(filename2, deletedFile2, sword);
                dictionary_list = dcm.insertFromFile(filename2);
                dictionary_deleted = dcm.insertFromFile2(deletedFile2);
                try {
                    dcm.DeleteAWordInFile2(favoriteFile2, word);
                    dcm.DeleteAWordInFile2(historyFile2, word);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            //giao dien.
            Result_WebView.getEngine().loadContent(" ");
            Search_Text_Field.setText(" ");
            LikeButton.setVisible(false);
            Sound_Button.setVisible(false);
            Search_ListView.setVisible(false);
            DeleteWord.setVisible(false);

        });

        //favorite tab.

        List_View_Favorite.setOnMouseClicked(event -> {
            String Chosen_Word = List_View_Favorite.getSelectionModel().getSelectedItem();
            Search_A_Word(Chosen_Word, dcm, Result_tab);
        });

        ChangeDicButton1.setOnMouseClicked(event -> {
            if (TypeDictionary1.getText().equals("Anh-Việt")) {
                TypeDictionary1.setText("Việt-Anh");
                state = 1;
                dictionary_list = dcm.insertFromFile(filename2);
                dcm.addFavoriteWordFromFile(favoriteFile2, dictionary_list);
                dictionary_favorite = dcm.insertFromFile2(favoriteFile2);
            } else {
                TypeDictionary1.setText("Anh-Việt");
                state = 0;
                dictionary_list = dcm.insertFromFile(filename);
                dcm.addFavoriteWordFromFile(favoriteFile, dictionary_list);
                dictionary_favorite = dcm.insertFromFile2(favoriteFile);
            }
            List_View_Favorite.getItems().clear();
            List_View_Favorite.getItems().addAll(dictionary_favorite);
            Result_tab.getEngine().loadContent(" ");
        });

        Sound_Button.setOnMouseClicked(event -> {
            String word = Search_Text_Field.getText();
            Word word1 = dcm.SearchNewWord(word, dictionary_list);
            if (state == 0) {
                dcm.speak(word1.getWord_target());
            } else {
                dcm.speak(word1.getExplain());
            }
        });

        Sound_FT.setOnMouseClicked(event -> {
            String word = List_View_Favorite.getSelectionModel().getSelectedItem();
            Word word1 = dcm.SearchNewWord(word, dictionary_list);
            if (state == 0) {
                dcm.speak(word1.getWord_target());
            } else {
                dcm.speak(word1.getExplain());
            }
        });

        Save_AddTab.setOnMouseClicked(event -> {
            String text = AddWord_Target.getText();
            Word aWord = getStateAddWord(text);
            System.out.println(aWord);
            System.out.println(text);
            if (text.length() > 0) {
                AddWord_Result.getEngine().loadContent(aWord.getWord_explain());
                if (state == 0) {
                    try {
                        dcm.DeleteAWordInFile(filename, aWord);
                        dcm.addWordToFile(filename, aWord, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    dictionary_list = dcm.insertFromFile(filename);
                    System.out.println(dcm.Dictionary_Lookup(text, dictionary_list));
                } else {
                    try {
                        dcm.DeleteAWordInFile(filename2, aWord);
                        dcm.addWordToFile(filename2, aWord, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    dictionary_list = dcm.insertFromFile(filename2);

                }

            } else {
                AddWord_Result.getEngine().loadContent("Bạn chưa nhập từ nào hãy nhập lại");
            }
            System.out.println("?");

        });

        GGDich.setOnMouseClicked(event -> {
            Chosen_Button(MenuTab, GGDich);
            Initial(tablist, GGTab);
            state = 0;
        });

        GGDich_Change.setOnMouseClicked(event -> {
            if (state == 0) {
                state = 1;
                GGDichText.setText("Tiếng Việt");
                GGDichText2.setText("Tiếng Anh");
                GGDichNGhia.setText(dcm.GGTranslate(GGDichNhap.getText(), "en"));
            } else {
                state = 0;
                GGDichText.setText("Tiếng Anh");
                GGDichText2.setText("Tiếng Việt");
                GGDichNGhia.setText(dcm.GGTranslate(GGDichNhap.getText(), "vi"));
            }
        });

        GGDichNhap.setOnKeyReleased(event -> {
            if (GGDichNhap.getLength() != 0) {

                String Search_Word = GGDichNhap.getText();
                if (state == 0) {
                    GGDichNGhia.setText(dcm.GGTranslate(Search_Word, "vi"));
                }
                else {
                    GGDichNGhia.setText(dcm.GGTranslate(Search_Word, "en"));
                }
            } else {
                GGDichNGhia.setText("");
            }
        });

        GG_Sound.setOnMouseClicked(event -> {
            String sword = GGDichNhap.getText();
            dcm.speak(sword);
        });

        GG_Sound2.setOnMouseClicked(event -> {
            String sword = GGDichNGhia.getText();
            dcm.speak(sword);
        });

    }


    // tim kiem 1 tu, them vao danh sach da tim kiem, kiemm tra co trong danh sach yeu thich khong.
    public void Search_A_Word(String SWord, DictionaryManagement dcm, WebView webView) {
        Search_Text_Field.setText(SWord);
        Search_ListView.setVisible(false);
        Search_Cancel.setVisible(true);
        FixWord.setVisible(true);
        Word SearchWord = dcm.SearchNewWord(SWord, dictionary_list);
        webView.getEngine().loadContent(SearchWord.getWord_explain());
        webView.setVisible(true);
        if (SearchWord.isLiked()) {
            LikeButtonColor.setFill(Color.RED);
        } else {
            LikeButtonColor.setFill(Color.BLACK);
        }
        if (!SearchWord.getWord_explain().equals("không có từ này")) {
            Sound_Button.setVisible(true);
            DeleteWord.setVisible(true);
            LikeButton.setVisible(true);
            dcm.addToList2(SWord, dictionary_history);
            if (state == 0) {
                try {
                    dcm.addWordToFile2(historyFile, SWord, 2);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    dcm.addWordToFile2(historyFile2, SWord, 2);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            Sound_Button.setVisible(false);
            DeleteWord.setVisible(false);
            LikeButton.setVisible(false);
        }

    }

    public void Search_A_Word2(String word, DictionaryManagement dcm, WebView webView) {
        webView.getEngine().loadContent("");
        String s = dcm.Dictionary_Lookup(word, dictionary_list);
        if ( s.equals("không có từ này")) {
            s = "chưa có từ này!";
        }
        webView.getEngine().loadContent(s);
    }



    //khoi tao khi chuyen menu.
    public void Initial(ArrayList<AnchorPane> tablist, AnchorPane tab) {
        for (AnchorPane anchorPane : tablist) {
            anchorPane.setVisible(false);
        }
        tab.setVisible(true);
        if (tab == Top_App) {
            Search_Text_Field.setText("");
            Search_ListView.setVisible(false);
            Search_Cancel.setVisible(false);
            Result_WebView.getEngine().loadContent("");
            Sound_Button.setVisible(false);
            DeleteWord.setVisible(false);
            LikeButton.setVisible(false);
            FixWord.setVisible(false);
            Result_WebView.setVisible(true);
            dictionary_backup = new ArrayList<>();
            indexBack = 0;
        }
        if (tab == AddWordTab) {
            TypeDictionary.setText("Anh-Việt");
            AddWord_Target.setText("");
            Explain1.setText("");
            Explain2.setText("");
            Explain3.setText("");
            AdjCheck.setSelected(false);
            NCheck.setSelected(false);
            VCheck.setSelected(false);
            AdvCheck.setSelected(false);
        }
        if (tab == FavoriteWord) {
            //sothing later.
            TypeDictionary1.setText("Anh-Việt");
            List_View_Favorite.setVisible(true);
            Result_tab.getEngine().loadContent(" ");
            List_View_Favorite.getItems().clear();
            List_View_Favorite.getItems().addAll(dictionary_favorite);
        }

        if (tab == GGTab) {
            GGDichNhap.setText("");
            GGDichNGhia.setText("");
            GGDichText.setText("Tiếng Anh");
            GGDichText2.setText("Tiếng Việt");
        }

    }

    // sau khi chuyen menu.
    public void Chosen_Button(ArrayList<Button> buttons, Button em) {
        for (Button button : buttons) {
            button.setStyle("-fx-background-color: #e7e7e7;");
        }
        em.setStyle("-fx-background-color:  #ffffff;");
    }

    //goi y tim kiem.
    public void recommendTask(String searchWord, int count, ListView<String> listview) {
        listview.getItems().clear();
        listview.getItems().addAll(dcm.dictionarySearcher(searchWord, count));
        listview.setVisible(true);
        if (listview.getItems().size() == 0) {
            listview.setVisible(false);
        }
    }

    public void deleteAWord(String filename, String deletedFile, Word aWord) {

        try {
            dcm.DeleteAWordInFile(filename, aWord);
            dcm.addWordToFile2(deletedFile, aWord.getWord_target(), 2);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Word getStateAddWord(String aWord) {
        Word word = new Word();
        word.setWord_target(aWord);
        String wordE = "<html><i>" + AddWord_Target.getText() + "</i><br/><ul><li><b><i>";
        if (AdjCheck.isSelected()) {
            wordE += "tính từ";
        }
        if (VCheck.isSelected()) {
            wordE += ", động từ";
        }
        if (NCheck.isSelected()) {
            wordE += ", danh từ";
        }
        if (AdvCheck.isSelected()) {
            wordE += ", trạng từ";
        }
        wordE += "</i></b><ul><li><font color='#cc0000'><b> " + Explain1.getText() + ", " + Explain2.getText() + ", " + Explain3.getText();
        wordE += "</b></font></li></ul></li></ul></html>";
        word.setWord_explain(wordE);

        return word;
    }



}
