package main.java.DictionaryClass;

import java.util.ArrayList;

public class Dictionary {

    public static ArrayList<Word> dictionary_list = new ArrayList<>();

    public static ArrayList<String> dictionary_history = new ArrayList<>();

    public static ArrayList<Word> dictionary_backup = new ArrayList<>();

    public static ArrayList<String> dictionary_favorite = new ArrayList<>();

    public static ArrayList<String> dictionary_deleted = new ArrayList<>();

    public static int indexBack = 0;

}
