package main.java.DictionaryClass;

import com.darkprograms.speech.synthesiser.SynthesiserV2;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.advanced.AdvancedPlayer;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static main.java.DictionaryClass.Dictionary.*;
import com.darkprograms.speech.translator.GoogleTranslate;

public class DictionaryManagement {


    //load du lieu tu file.
    public ArrayList<Word> insertFromFile(String filename) {
        ArrayList<Word> list = new ArrayList<>();
        try {
            File file = new File(filename);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;

            while((line = bufferedReader.readLine()) != null){

                int count = 0;
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) == '<') {
                        count = i;
                        break;
                    }
                }
                String word_target = line.substring(0, count);
                String word_explain = line.substring(count);
                Word addWord = new Word(word_target, word_explain);
                list.add(addWord);
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return list;
    }

    public ArrayList <String> insertFromFile2(String filename) {

        ArrayList<String> list = new ArrayList<>();
        try {
            File file = new File(filename);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;

            while((line = bufferedReader.readLine()) != null){

                list.add(line);
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return list;

    }

    public void addFavoriteWordFromFile(String filename, ArrayList<Word> list) {
        ArrayList<String> words = insertFromFile2(filename);
        int index;
        if (words.size() != 0) {
            for (String word : words) {
                index = binarySearch(list, word, 2);
                if (index >= 0) {
                    list.get(index).setLiked(true);
                }

            }
        }
    }

    //sắp xếp lại.
    public void sort_dictionary_list (ArrayList<Word> list) {
        list.sort(Comparator.comparing(Word::getWord_target));
    }

    //tra từ.

    public String Dictionary_Lookup(String SearchWord, ArrayList<Word> list){

        int index_binary = binarySearch(list, SearchWord, 2);
        if (index_binary < 0) {
            return "không có từ này";
        } else {
            return list.get(index_binary).getWord_explain();
        }
    }

    //tra tu tra ve 1 doi tuong word
    public Word SearchNewWord(String SearchWord, ArrayList<Word> list) {
        int index_binary = binarySearch(list, SearchWord, 2);
        if (index_binary < 0) {
            return new Word("......", "không có từ này");
        } else {
            return list.get(index_binary);
        }
    }

    // tra ve cac tu goi y.
    public ArrayList<String> dictionarySearcher(String searchWord, int count) {
        int curr = fistChar(dictionary_list, searchWord);
        ArrayList<String> results = new ArrayList<>();

        if (curr == -1) {
            results = new ArrayList<>();
        } else {
            int size = curr + count;
            if (size > dictionary_list.size()) {
                size = dictionary_list.size();
            }
            for (int i = curr; i < size; i++) {
                if (check_word(searchWord, dictionary_list.get(i).getWord_target())) {
                    results.add(dictionary_list.get(i).getWord_target());
                }
            }
        }
        return results;
    }

    //them vao danh sach da tim trc do.
    public void addWordBackList(Word new_word) {
        if (dictionary_backup.size() == 0) {
            indexBack = 0;
            dictionary_backup.add(new_word);
        } else {
            if (!dictionary_backup.get(indexBack).getWord_target().equals(new_word.getWord_target())) {
                for (int i = indexBack + 1; i < dictionary_backup.size() - 1; i++) dictionary_backup.remove(i);
                dictionary_backup.add(new_word);
                indexBack ++;
            }
        }
    }

    /**
     * binarySearch.
     */
    // type 1 : tim ki tu dau tien.
    // type con lai : tim tu chinh xac dau tien.
    private int binarySearch(ArrayList<Word> words, String s, int type) {
        if (s == null) {
            return -1;
        }
        int sizeOfWord = s.length();
        int left = 0;
        int right = words.size() - 1;
        while (left <= right) {
            int mid = (left + right) / 2;
            int sizeofWord2 = words.get(mid).getWord_target().length();
            String midword = words.get(mid).getWord_target();

            if (type == 1) {
                if (sizeofWord2 > sizeOfWord) {
                    midword = midword.substring(0, sizeOfWord);
                }
            }
            if (s.compareTo(midword) == 0) {
                return mid;
            }
            if (s.compareTo(midword) > 0) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return -1;
    }

    /**
     * tu dau tien.
     */
    private int fistChar(ArrayList<Word> words, String s) {
        int curr = binarySearch(words, s, 1);
        if (curr == -1) {
            return -1;
        } else {
            while (curr > 0 && check_word(s, words.get(curr).getWord_target())) {
                curr -= 2;
            }
            if (curr < 0) {
                curr = 0;
            }
            if (check_word(s, words.get(curr + 1).getWord_target())) {
                curr = curr + 1;
            } else {
                if (check_word(s, words.get(curr + 2).getWord_target())) {
                    curr = curr + 2;
                }
            }
        }
        return curr;
    }

    //so sanh 2 tu.
    private boolean check_word(String s1, String s2) {
        if (s1.length() > s2.length()) {
            return false;
        } else {
            return s1.equals(s2.substring(0, s1.length()));
        }
    }

    //luu mang tu dien hien tai sang file.
    private void ExportToFile(String filename,ArrayList<Word> words) throws IOException {

        FileWriter fileWriter = new FileWriter(filename);
        BufferedWriter buff = new BufferedWriter(fileWriter);
        String line = "\r\n";
        for (Word word : words) {
            String word_target = word.getWord_target();
            buff.write(word_target);
            String word_explain = word.getWord_explain();
            buff.write(word_explain);
            buff.write(line);
        }
        buff.close();
        fileWriter.close();
    }

    private void ExportToFile2(String filename, ArrayList<String> list) throws IOException {

        FileWriter fileWriter = new FileWriter(filename);
        BufferedWriter buff = new BufferedWriter(fileWriter);
        String line = "\r\n";
        for (String s : list) {
            buff.write(s);
            buff.write(line);
        }
        buff.close();
        fileWriter.close();

    }

    //xoa file.
    private void deletefile(String filename) {
        File file = new File(filename);
        file.delete();
    }

    //them tu moi vao file.

    //String word_explain2 = "<html><i>" + addWord.getWord_target() + "</i><br/><ul><li><b><i>" + addWord.getType() + "</i></b><ul><li><font color='#cc0000'><b> " + addWord.getWord_explain() + "</b></font></li></ul></li></ul></html>";.
    //type = 1 : sap xep truoc khi them vao, type # k sap xep chi gioi han 20 tu.
    public void addWordToFile(String filename, Word addWord, int type) throws IOException {
        ArrayList<Word> list;
        list = insertFromFile(filename);

        if (type == 1) {
            list.add(addWord);
            sort_dictionary_list(list);
        } else {
            addToList(addWord, list);
        }
        deletefile(filename);
        ExportToFile(filename,list);
    }

    public void addWordToFile2(String filename, String sword, int type) throws IOException {
        ArrayList<String> list;
        list = insertFromFile2(filename);

        if (type == 1) {
            list.add(sword);
            Collections.sort(list);
        } else {
            addToList2(sword, list);
        }
        deletefile(filename);
        ExportToFile2(filename,list);
    }

    public void DeleteAWordInFile(String filename, Word DWord) throws IOException{
        ArrayList<Word> list;
        list = insertFromFile(filename);
        int index = binarySearch(list, DWord.getWord_target(), 2);
        if (index >= 0) {
            list.remove(index);
            deletefile(filename);
            ExportToFile(filename, list);
        }
    }

    public void DeleteAWordInFile2(String filename, String Dword) throws IOException {
        ArrayList<String> list;
        list = insertFromFile2(filename);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(Dword)) {
                list.remove(i);
            }
        }
        deletefile(filename);
        ExportToFile2(filename, list);
    }


    public void addToList(Word sword, ArrayList<Word>list) {

        if (list.size() > 20) {
            list.remove(20);
        }
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getWord_target().equals(sword.getWord_target())) {
                list.remove(i);
            }
        }
        list.add(sword);
    }

    public void addToList2(String sword, ArrayList<String> list) {
        if (list.size() > 20) {
            list.remove(20);
        }
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(sword)) {
                list.remove(i);
            }
        }
        list.add(sword);
    }

    public void addLikedWord(String searchWord, ArrayList<Word>list, ArrayList<String> list2) {
        int index = binarySearch(list, searchWord, 2);
        list.get(index).setLiked(true);
        addToList2(list.get(index).getWord_target(), list2);
        Collections.sort(list2);
    }

    public void removeLikedWord(String sword, ArrayList<Word>list, ArrayList<String> favorList) {
        int index = binarySearch(list, sword, 2);
        list.get(index).setLiked(false);
        for (int i = 0; i < favorList.size(); i++) {
            if (favorList.get(i).equals(sword)) {
                favorList.remove(i);
            }
        }
    }


//    //speech.

    public void speak(String text) {
        SynthesiserV2 synthesizer = new SynthesiserV2("AIzaSyBOti4mM-6x9WDnZIjIeyEU21OpBXqWBgw");

        //Create a new Thread because JLayer is running on the current Thread and will make the application to lag
        Thread thread = new Thread(() -> {
            try {
                //Create a JLayer instance
                AdvancedPlayer player = new AdvancedPlayer(synthesizer.getMP3Data(text));
                player.play();

            } catch (IOException | JavaLayerException e) {

                e.printStackTrace(); //Print the exception ( we want to know , not hide below our finger , like many developers do...)

            }
        });

        //We don't want the application to terminate before this Thread terminates
        thread.setDaemon(false);

        //Start the Thread
        thread.start();

    }

    public String GGTranslate(String sWord, String language) {
        try {
            return GoogleTranslate.translate(language, sWord);
        } catch (IOException e) {
            return sWord;
        }
    }

}
