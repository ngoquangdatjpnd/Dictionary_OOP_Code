package main.java.DictionaryClass;

import java.io.FileNotFoundException;

import static main.java.DictionaryClass.Dictionary.dictionary_list;

public class DictionaryCommandline {

    public void showAllWords() {
        int size = dictionary_list.size();
        for (int i = 0; i < size; i++) {
            System.out.println(dictionary_list.get(i).getWord_target() + " : " + dictionary_list.get(i).getWord_explain());
        }
    }

    public void dictionaryBasic() {
        DictionaryManagement dcm = new DictionaryManagement();
        dcm.sort_dictionary_list(dictionary_list);
        showAllWords();
    }
    public void DictionaryAdvance() throws FileNotFoundException{
        DictionaryManagement dcm = new DictionaryManagement();
//        dcm.addWordInList();
        dictionary_list = dcm.insertFromFile("rsrc\\Text_file\\E_V.txt");
        showAllWords();
        dcm.Dictionary_Lookup("zymotic", dictionary_list);
        System.out.println(dcm.dictionarySearcher("e", 10));

    }


}
