package main.java.DictionaryClass;

public class Word {
    private String word_target;
    private String word_explain;
    private boolean liked;

    public Word() {
        this.word_target = "";
        this.word_explain = "";
    }

    public Word(String wt, String we) {
        this.word_target = wt;
        this.word_explain = we;
        this.liked = false;
    }


    public void setWord_target(String word_target) {
        this.word_target = word_target;
    }

    public void setWord_explain(String word_explain) {
        this.word_explain = word_explain;
    }

    public String getWord_target() {
        return word_target;
    }

    public String getWord_explain() {
        return word_explain;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    @Override
    public String toString() {
        return this.word_target + " : " + this.word_explain;
    }

    public String getExplain() {
        String s, text = word_explain;
        int count1 = 0, count2 = 0;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == 'i' && text.charAt(i+1) == '>') {
                count1 = i + 2;
            }
            if (text.charAt(i) == '/' && text.charAt(i + 1) == 'i') {
                count2 = i - 1;
                break;
            }
        }
        s = text.substring(count1, count2);
        return s;
    }

}
